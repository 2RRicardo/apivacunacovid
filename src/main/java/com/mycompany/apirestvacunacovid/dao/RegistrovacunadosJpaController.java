/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apirestvacunacovid.dao;

import com.mycompany.apirestvacunacovid.dao.exceptions.NonexistentEntityException;
import com.mycompany.apirestvacunacovid.dao.exceptions.PreexistingEntityException;
import com.mycompany.apirestvacunacovid.entity.Registrovacunados;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Ricardo
 */
public class RegistrovacunadosJpaController implements Serializable {

    public RegistrovacunadosJpaController() {
       
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Registrovacunados registrovacunados) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(registrovacunados);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRegistrovacunados(registrovacunados.getRut()) != null) {
                throw new PreexistingEntityException("Registrovacunados " + registrovacunados + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Registrovacunados registrovacunados) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            registrovacunados = em.merge(registrovacunados);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = registrovacunados.getRut();
                if (findRegistrovacunados(id) == null) {
                    throw new NonexistentEntityException("The registrovacunados with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Registrovacunados registrovacunados;
            try {
                registrovacunados = em.getReference(Registrovacunados.class, id);
                registrovacunados.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The registrovacunados with id " + id + " no longer exists.", enfe);
            }
            em.remove(registrovacunados);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Registrovacunados> findRegistrovacunadosEntities() {
        return findRegistrovacunadosEntities(true, -1, -1);
    }

    public List<Registrovacunados> findRegistrovacunadosEntities(int maxResults, int firstResult) {
        return findRegistrovacunadosEntities(false, maxResults, firstResult);
    }

    private List<Registrovacunados> findRegistrovacunadosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Registrovacunados.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Registrovacunados findRegistrovacunados(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Registrovacunados.class, id);
        } finally {
            em.close();
        }
    }

    public int getRegistrovacunadosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Registrovacunados> rt = cq.from(Registrovacunados.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
