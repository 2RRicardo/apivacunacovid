/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apirestvacunacovid.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ricardo
 */
@Entity
@Table(name = "registrovacunados")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Registrovacunados.findAll", query = "SELECT r FROM Registrovacunados r"),
    @NamedQuery(name = "Registrovacunados.findByNombre", query = "SELECT r FROM Registrovacunados r WHERE r.nombre = :nombre"),
    @NamedQuery(name = "Registrovacunados.findByDosis1", query = "SELECT r FROM Registrovacunados r WHERE r.dosis1 = :dosis1"),
    @NamedQuery(name = "Registrovacunados.findByFecha1", query = "SELECT r FROM Registrovacunados r WHERE r.fecha1 = :fecha1"),
    @NamedQuery(name = "Registrovacunados.findByDosis2", query = "SELECT r FROM Registrovacunados r WHERE r.dosis2 = :dosis2"),
    @NamedQuery(name = "Registrovacunados.findByFecha2", query = "SELECT r FROM Registrovacunados r WHERE r.fecha2 = :fecha2"),
    @NamedQuery(name = "Registrovacunados.findByRut", query = "SELECT r FROM Registrovacunados r WHERE r.rut = :rut")})
public class Registrovacunados implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "dosis1")
    private String dosis1;
    @Size(max = 2147483647)
    @Column(name = "fecha1")
    private String fecha1;
    @Size(max = 2147483647)
    @Column(name = "dosis2")
    private String dosis2;
    @Size(max = 2147483647)
    @Column(name = "fecha2")
    private String fecha2;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;

    public Registrovacunados() {
    }

    public Registrovacunados(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDosis1() {
        return dosis1;
    }

    public void setDosis1(String dosis1) {
        this.dosis1 = dosis1;
    }

    public String getFecha1() {
        return fecha1;
    }

    public void setFecha1(String fecha1) {
        this.fecha1 = fecha1;
    }

    public String getDosis2() {
        return dosis2;
    }

    public void setDosis2(String dosis2) {
        this.dosis2 = dosis2;
    }

    public String getFecha2() {
        return fecha2;
    }

    public void setFecha2(String fecha2) {
        this.fecha2 = fecha2;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Registrovacunados)) {
            return false;
        }
        Registrovacunados other = (Registrovacunados) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.apirestvacunacovid.entity.Registrovacunados[ rut=" + rut + " ]";
    }
    
}
