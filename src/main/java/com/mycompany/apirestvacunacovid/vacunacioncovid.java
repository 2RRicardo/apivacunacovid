/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apirestvacunacovid;

import com.mycompany.apirestvacunacovid.dao.RegistrovacunadosJpaController;
import com.mycompany.apirestvacunacovid.dao.exceptions.NonexistentEntityException;
import com.mycompany.apirestvacunacovid.entity.Registrovacunados;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Ricardo
 */
@Path("registrovacunados")
public class vacunacioncovid {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listavacunados() {

        /* Registrovacunados vc = new Registrovacunados();
        vc.setNombre("juan");
        vc.setRut("10-9");
         vc.setDosis1("Si");
          vc.setFecha1("10-9-2021");
           vc.setDosis2("No");
            vc.setFecha2("10-9-2022");
        
        Registrovacunados vc2=new Registrovacunados();
        vc2.setNombre("pepe");
        vc2.setRut("10-0");
        vc.setDosis1("Si");
          vc.setFecha1("10-9-2021");
           vc.setDosis2("No");
            vc.setFecha2("10-9-2022");
        
        List<Registrovacunados> lista = new ArrayList();
        
        lista.add(vc);
        lista.add(vc2);*/
        RegistrovacunadosJpaController dao = new RegistrovacunadosJpaController();

        List<Registrovacunados> lista = dao.findRegistrovacunadosEntities();

        return Response.ok(200).entity(lista).build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response Add(Registrovacunados regvacunado) {

        try {
            RegistrovacunadosJpaController dao = new RegistrovacunadosJpaController();
            dao.create(regvacunado);
        } catch (Exception ex) {
            Logger.getLogger(vacunacioncovid.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(regvacunado).build();
    }

    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete") String iddelete) {
        try {
            RegistrovacunadosJpaController dao = new RegistrovacunadosJpaController();
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(vacunacioncovid.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("Usuario Eliminado").build();   
    }
    
    @PUT
    public Response update(Registrovacunados regvacunado){
        try {
            RegistrovacunadosJpaController dao = new RegistrovacunadosJpaController();
            dao.edit(regvacunado);
        } catch (Exception ex) {
            Logger.getLogger(vacunacioncovid.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("Usuario actualizado").build();   
    }
    
    

}
